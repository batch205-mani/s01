let students = [
	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"	
];

// console.log(students);


// class Dog {
// 	constructor(name,breed,age){
// 		this.name = name;
// 		this.breed = breed;
// 		this.age = age;
// 	}
// }

// let dog1 = new Dog("Bantay","corgi",3);

// console.log(dog1);

// let person1 = {
// 	name: "Saitama",
// 	heroName: "One Punch Man",
// 	age: 30
// };

// console.log(person1);

// let allDivisible;
// let arrNum = [15,25,50,20,10,11];

// arrNum.forEach(num => {

// 	if(num % 5 === 0){
// 		console.log(`${num} is divisible by 5.`)
// 	} else {
// 		allDivisible = false;
// 	}

// })	

// console.log(allDivisible);

// let divisibleBy5 = arrNum.every(num => {
// 	console.log(num);
// 	return num % 5 === 0;
// })

// console.log(divisibleBy5);

// *********** ACTIVITY QUIZ **********
/* 
1. by using Square Bracket ;
2. accesing it by using the indexes. and the arrays starts in zero index.
3. array.length - 1
4. .indexOf
5. forEach()
6. .map()
7. .every()
8. .some()
9. False
10. True
11. False
12. Math
13. Yes, but strings are not object. but when methods are being used js temporarily converting it to objects. thus, making it an object temporarily.
14. False
15. True?

****** FUNCTION CODING **********
*/
// 1.
function addToEnd(students, name){
	if((typeof name) === "string"){
		students.push(name)
		console.log(students)
	} else {
		console.log("error - can only add strings to an array")
	}

}

addToEnd(students,"Ryan");
addToEnd(students,045);

//2.
function addToStart(students, name){
	if((typeof name) === "string"){
		students.unshift(name)
		console.log(students)
	} else {
		console.log("error - can only add strings to an array")
	}	
}

addToStart(students,"Tess");
addToStart(students,033);

//3
function elementChecker(students, name){
	let studCheck = students.some(num=> {
		return num === name
	});
	if(students.length > 0){
		console.log(studCheck);
	} else {
		console.log("error - passed in array is empty")	
	}
}

elementChecker(students, "Jane");
elementChecker([], "Jane");

//4
function stringLengthSorter(students){
	let checkStrings = students.every(num => {
		return typeof num === "string" ;
	});

	if(checkStrings){
		let newStudents = students.sort((a,b) => a.length - b.length);
		console.log(newStudents);
	} else {
		console.log("error - all array must be strings")
	}
	
}

stringLengthSorter(students);
stringLengthSorter([037, "John", 039, "Jane"]);
